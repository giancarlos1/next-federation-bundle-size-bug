import '@/styles/globals.css'
import type { AppProps } from 'next/app'

const App: React.FC<AppProps> = ({Component: CustomComponent, pageProps}) => {
  // @ts-ignore
  return <CustomComponent {...pageProps} />
}

export default App