const NextFederationPlugin = require('@module-federation/nextjs-mf');

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  webpack: (config, options) => {
    // webpack module federation
    if (!options.isServer) {
      config.plugins.push(
        new NextFederationPlugin({
          name: 'customRemote',
          remotes: {
            crehLXMF:
              'crehLXMF@http://localhost:3004/_next/static/chunks/remoteEntry.js',
          },
          filename: 'static/chunks/remoteEntry.js',
          exposes: {
            './pages-map': './src/pages-map.js',
          },
          // shared: {},
        }),
      );
    }

    return config;
  }
}

module.exports = nextConfig
